<!---
Suggested title: Threat Management Community Office Hours - 2021-MM-DD HH:MM UTC (TBC)

Fields to fill-in:
- line 28: YYYY-MM-DD HHMM
- line 29: YYYYMMDDTHHMM (example: iso=20210828T070000)
- line 30: ZOOM_LINK, password
- line 31: CALENDAR_LINK (example: https://calendar.google.com/event?action=TEMPLATE&tmeid=MzY3dmJkM2xvNmpzMTY5OTk5NnY2ZjJsNjAgY191c2MyNGJ0MHNvZ3Q0N2FoMDJxMHFkN    Dhra0Bn&tmsrc=c_usc24bt0sogt47ah02q0qd48kk%40group.calendar.google.com)
- line 38: agenda ITEMs
--->

This is a meta issue to capture [topics and questions to be discussed](#agenda) during the
community office hours [event](#meeting-details).

Recordings of the call will be posted to the [Community Office Hours YouTube
Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrmpGUt33tVXzONJiwCgb_S)

In Threat Management community office hours, we cover topics from these categories:

- ~"Category:Vulnerability Management" 
- ~"Category:Container Host Security" 
- ~"Category:Container Network Security"

Please add your suggested topic as a comment and a GitLab team member will update the agenda
accordingly.


## Meeting details

- When: YYYY-MM-DD HHMM UTC ([Find your local
  time](https://www.timeanddate.com/worldclock/converter.html?iso=YYYYMMDDTHHMM00&p1=1440&p2=tz_pt&p3=136&p4=tz_cest&p5=240)).
- ZOOM_LINK (Password: password)
- [Add Event to your Google Calendar](CALENDAR_LINK)


## Agenda

In draft until we set the date/time.

1. ITEM 1
1. ITEM 2

## Checklist for a successful event

1. [ ] Assign this issue to a DRI who will host the session.
1. [ ] Confirm the session date and time.
   1. Update description and links.
   1. Confirm there are no [other events](https://gitlab.com/gitlab-org/threat-management/general/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Office%20Hours)
      scheduled for the same week.
1. [ ] Advertise the event.
   1. [ ] [Gitter](https://gitter.im/gitlab/contributors).
   1. [ ] Directly invite previous community contributors by tagging them in this issue.
   1. [ ] Social media.
1. [ ] Fill-out the agenda items.
   1. If there are no suggested topics, pre-select issues for refinement and/or pair programming.

/label ~"Office Hours"  

